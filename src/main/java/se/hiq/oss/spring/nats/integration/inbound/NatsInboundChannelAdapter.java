package se.hiq.oss.spring.nats.integration.inbound;


import org.springframework.context.SmartLifecycle;
import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.messaging.support.MessageBuilder;

import io.nats.client.Message;

import static se.hiq.oss.spring.nats.integration.NatsHeaders.*;

import se.hiq.oss.spring.nats.consumer.NatsConsumerManager;


public class NatsInboundChannelAdapter extends MessageProducerSupport {

    private NatsConsumerManager natsConsumerManager;
    private Class<?> requestPayloadType;
    private String subject;
    private String queueName = "";
    private SmartLifecycle dispatcher;


    @Override
    public void onInit() {
        super.onInit();
        if (natsConsumerManager == null) {
            natsConsumerManager = this.getBeanFactory().getBean(NatsConsumerManager.class);
        }
        dispatcher = natsConsumerManager.register(this::onMessage, requestPayloadType, subject, queueName);
    }


    @Override
    protected void doStart() {
        dispatcher.start();
    }


    @Override
    protected void doStop() {
        dispatcher.stop();
    }


    protected org.springframework.messaging.Message<Object> buildMessage(Object object, Message msg) {
        return MessageBuilder.withPayload(object)
                .setHeader(SUBJECT, msg.getSubject())
                .setHeader(QUEUE, queueName)
                .setHeader(SUBSCRIPTION, msg.getSubscription())
                .setHeader(SUBSCRIPTION_ID, msg.getSID())
                .setHeader(REPLY_TO, msg.getReplyTo())
                .build();
    }

    private void onMessage(Object object, Message msg) {
        org.springframework.messaging.Message<Object> message = buildMessage(object, msg);
        try {
            this.sendMessage(message);
        } catch (RuntimeException e) {
            this.sendErrorMessageIfNecessary(message, e);
        }

    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }


    public void setRequestPayloadType(Class<?> requestPayloadType) {
        this.requestPayloadType = requestPayloadType;
    }

    public void setNatsConsumerManager(NatsConsumerManager natsConsumerManager) {
        this.natsConsumerManager = natsConsumerManager;
    }
}
