package se.hiq.oss.spring.nats.integration.integration;

import io.nats.client.Message;

import se.hiq.oss.spring.nats.NatsTemplate;
import se.hiq.oss.spring.nats.annotation.Consumer;

public class ExternalNatsPersonServiceMock {

    private PersonService personService;
    private NatsTemplate template;

    @Consumer(subject = "person-subject")
    public void onPerson(Person person, Message message) {
        template.publish(personService.onPerson(person), message.getReplyTo());
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public void setTemplate(NatsTemplate template) {
        this.template = template;
    }
}
